from django import forms


class LoginForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput(),
    )


class SignupForm(forms.Form):
    username = forms.CharField(max_length=150, initial="")
    password = forms.CharField(
        max_length=150, widget=forms.PasswordInput(), initial=""
    )
    password_confirmation = forms.CharField(
        max_length=150, widget=forms.PasswordInput(), initial=""
    )
